# `op-frontend`
The GUI interface of the **op-monitor** project.

Using the `op-web-api` to display the data and resolve actions at the different components.

## Views:

### Home View
* The default page, explain the project terminology and navigate through the site.
    
### Agents-View:
* Display the active Agents, allowing to add or remove.
    
### Agent-View: 
* Display all the monitoring processes of the selected Agent.
* An Agent might support several Monitors.

### Monitors-Views
* Display the active Monitors, allowing to add or remove exsiting.
    
### Monitor View
* Dislpay a requested Monitor, and it's registered Agents.
* Enable to register or unregister exising Agent to this Monitor.
    

## Stack:
* React\Redux
* UI-lib: [material-ui](https://material-ui.com), [google-charts](https://www.npmjs.com/package/react-google-charts)


## DEMO
![Alt-Text](os-frontend-2.gif)
