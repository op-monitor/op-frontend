import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import configureStore from "./store/createStore";
import './index.css';

import AppContainer from './routes/MainLayout/containers/AppContainer';
import HomeContainer from './routes/MainLayout/containers/HomeContainer'
import MonitorsContainer from './routes/MainLayout/containers/MonitorsTableContainer'
import MonitorViewContainer from './routes/MainLayout/containers/MonitorViewContainer'
import AgentViewContainer from "./routes/MainLayout/containers/AgentViewContainer";
import NotFoundContainer from './routes/MainLayout/containers/NotFoundContainer'
import AgentsTableContainer from "./routes/MainLayout/containers/AgentsTableContainer";

import Startup from "./components/Startup/Startup";


ReactDOM.render(
    <Provider store={configureStore()}>
        <Startup>
            <Router>
                <AppContainer/>
                <Switch>
                    <Route exact path="/" component={HomeContainer}/>
                    <Route path="/home/" component={HomeContainer}/>
                    <Route path="/agents/" component={AgentsTableContainer}/>
                    <Route path="/monitors/" component={MonitorsContainer}/>
                    <Route path="/monitor/:monitorId" component={MonitorViewContainer}/>
                    <Route path="/agent/:agentId" component={AgentViewContainer}/>
                    <Route component={NotFoundContainer} />
                </Switch>
            </Router>
        </Startup>
    </Provider>, document.getElementById('root')
);