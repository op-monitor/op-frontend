import mainReducer from './MainLayout/mainReducer';
import coreReducer from './CoreLayout/coreReducer';

export {mainReducer, coreReducer};