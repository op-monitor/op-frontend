import {urls} from '../../../utils/'

const CHANGE_FIELD = 'CORE.CHANGE_FIELD';
const CLOSE_NOTIFICATION = 'CORE.CLOSE_NOTIFICATION';
const HANDLE_ERROR = 'CORE.HANDLE_ERROR';
const SHOW_INFO = 'CORE.SHOW_INFO';

export const errorMessage = 'Oops... Something went wrong. Please reload the page';

export const loadConfig = () => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            getApiUrl()
                .then(apiUrl => {
                    dispatch(changeField('urls', urls(apiUrl)));
                    resolve()
                })
                .catch(error => {
                    console.error(`Failed to load urls. Error: ${error}`);
                    reject()
                })
        })
    }
};

const getApiUrl = () => new Promise((resolve, reject) => {
    fetch('/apiUrl.txt', {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        cache: 'no-cache'
    })
        .then(resp => resolve(resp.text()))
        .catch(err => reject(err))
});

const changeField = (fieldName, value) => {
    return {
        type: CHANGE_FIELD,
        fieldName,
        value
    }
};

export function closeNotification() {
    return {
        type: CLOSE_NOTIFICATION
    }
}

export function handleError(error, showErrorMessage = false) {
    if (error.constructor === Response) {
        error = `Response ${error.status} ${error.statusText} at ${error.url}`
    }
    console.log(error);

    return {
        type: HANDLE_ERROR,
        error,
        showErrorMessage
    }
}

export function showInfo(message) {
    return {
        type: SHOW_INFO,
        message
    }
}

// TODO: add `fetching` loading bar
export const ACTION_HANDLERS = {
    [CHANGE_FIELD]: (state, action) => {
        const newState = {...state};
        newState[action.fieldName] = action.value;
        return newState;
    },
    [CLOSE_NOTIFICATION]: (state, action) => {
        return ({...state, isNotificationOpen: false})
    },
    [HANDLE_ERROR]: (state, action) => {
        return ({
            ...state,
            isNotificationOpen: true,
            fetching: false,
            notificationMessage: action.showErrorMessage ? action.error : errorMessage,
            notificationReason: 'error'
        })
    },
    [SHOW_INFO]: (state, action) => {
        return ({
            ...state,
            isNotificationOpen: true,
            fetching: false,
            notificationMessage: action.message,
            notificationReason: 'info'
        })
    },
};
