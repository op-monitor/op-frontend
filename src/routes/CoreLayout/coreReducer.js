import {ACTION_HANDLERS as coreActions} from "./actions/CoreActions";

const initialState = {
    urls: {},
    isNotificationOpen: false,
    notificationMessage: '',
    notificationReason: 'error',
};


export default (state = initialState, action) => {
    const ACTION_HANDLERS = {
        ...coreActions
    };
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}