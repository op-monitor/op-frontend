import {ACTION_HANDLERS as mainActions} from "./actions/mainActions";
import {ACTION_HANDLERS as monitorsActions} from "./actions/monitorsActions";
import {ACTION_HANDLERS as agentActions} from "./actions/agentsActions";
import {ACTION_HANDLERS as reportsActions} from "./actions/reportsActions";

const initialState = {
    isMenuOpen: false,
    monitors: [],
    agents: [],
    reports: [],
    monitor: {
        _id: null,
        port: 0,
        host: "",
        agents: [],
        creationDate: ""
    },
    agent: {
        _id: null,
        port: 0,
        host: "",
        monitors: [],
        creationDate: ""
    },
    report: {
        _id: null,
        date: Date(),
        monitor: {},
        agent: {},
        task: "",
        data: {}
    },
    modal: {
        host: '127.0.0.1',
        port: 9000,
    },
    isModalOpen: false
};

export default (state = initialState, action) => {
    const ACTION_HANDLERS = {
        ...mainActions,
        ...monitorsActions,
        ...agentActions,
        ...reportsActions
    };
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}