const TOGGLE_MENU = 'MAIN.TOGGLE_MENU';
const CLOSE_MODAL = 'MAIN.MODAL.CLOSE';
const OPEN_MODAL = 'MAIN.MODAL.OPEN';
const SET_MODAL_DATA = 'MODAL.SET_DATA';


export const toggleMenu = (value) => {
    return (dispatch, getState) => {
        if (typeof value !== "boolean") {
            const status = getState().main.isMenuOpen;
            value = !status
        }
        dispatch({type: TOGGLE_MENU, value});
    }
};

export const closeModal = () => {
    return {type: CLOSE_MODAL}
};

export const openModal = () => {
    return {type: OPEN_MODAL}
};

export const updateModalInputs = (field, value) => {
    return {
        type: SET_MODAL_DATA,
        field,
        value
    }
};

export const ACTION_HANDLERS = {
    [TOGGLE_MENU]: (state, action) => {
        const newState = {...state, isMenuOpen: action.value};
        return newState
    },
    [CLOSE_MODAL]: (state, action) => {
        const newState = {...state, isModalOpen: false};
        return newState
    },
    [OPEN_MODAL]: (state, action) => {
        const newState = {...state, isModalOpen: true};
        return newState
    },
    [SET_MODAL_DATA]: (state, action) => {
        const newState = {
            ...state,
            modal: {...state.modal, [action.field]: action.value}
        };
        return newState
    },
};