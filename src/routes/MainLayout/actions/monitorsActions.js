import {stateValueExtractor} from '../../../utils'
import {closeModal} from "./mainActions";
import {handleError, showInfo} from "../../CoreLayout/actions/CoreActions";


const SET_MONITORS = 'MONITOR.SET_MONITORS';
const SET_MONITOR_DATA = 'MONITOR.SET_MONITOR_DATA';
const ADD_MONITOR = 'MONITOR.ADD_MONITOR';
const DELETE_MONITOR = 'MONITOR.DELETE_MONITOR';
const ADD_AGENT_TO_MONITOR = 'MONITOR.REGISTER_AGENT';
const REMOVE_AGENT_FROM_MONITOR = 'MONITOR.UNREGISTER_AGENT';


export const getAllMonitors = () => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getMonitors(), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_MONITORS, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getAllMonitors', errorPayload);
            })
        });
    }
};

export const getMonitor = (id) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getMonitor(id), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_MONITOR_DATA, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getMonitor', errorPayload);
            })
        });
    }
};

export const addNewMonitor = () => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        const modalData = getState().main.modal;
        const packetData = {data: {host: modalData.host, port: Number(modalData.port)}};

        return new Promise((resolve, reject) => {
            fetch(urls.addMonitor(), {
                method: 'POST',
                body: JSON.stringify(packetData),
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: ADD_MONITOR, data: resp.data});
                dispatch(showInfo('Added new Monitor successfully'));
                dispatch(closeModal());
            }).catch((errorPayload) => {
                console.error('addMonitor', errorPayload);
                dispatch(handleError(errorPayload, true))
            })
        });
    }
};

export const deleteMonitor = (id) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        return new Promise((resolve, reject) => {
            fetch(urls.deleteMonitor(id),
                {
                    method: 'DELETE',
                    ...defaultSettings
                })
                .then((resp) => {
                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else {
                        throw resp
                    }
                })
                .then(resp => {
                    dispatch({type: DELETE_MONITOR, id: id});
                    dispatch(showInfo('Deleted Monitor successfully'));
                })
                .catch((errorPayload) => {
                    console.error('deleteMonitor', errorPayload);
                    dispatch(handleError(errorPayload, true))
                })
        });
    }
};

export const registerAgent = () => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        const {modal, monitor, agents} = getState().main;
        const selectedAgent = agents.find(a => a.host === modal.host && a.port === Number(modal.port));

        if (!selectedAgent) {
            dispatch(handleError("Agent doesn't exists in the system", true));
            console.error("Agent doesn't exists in the system");
            return;
        }

        return new Promise((resolve, reject) => {
            fetch(urls.registerAgentToMonitor(monitor._id, selectedAgent._id), {
                method: 'POST',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: ADD_AGENT_TO_MONITOR, agent: selectedAgent, mid: monitor._id});
                dispatch(showInfo('Registered Agent successfully'));
                dispatch(closeModal());
            }).catch((errorPayload) => {
                console.error('registerAgent', errorPayload);
                dispatch(handleError(errorPayload, true))
            })
        });
    }
};

export const unregisterAgent = (aid) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        const {monitor} = getState().main;

        return new Promise((resolve, reject) => {
            fetch(urls.unregisterAgentToMonitor(monitor._id, aid), {
                method: 'POST',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: REMOVE_AGENT_FROM_MONITOR, aid, mid: monitor._id});
                dispatch(showInfo('Unregistered Agent successfully'));
                dispatch(closeModal());
            }).catch((errorPayload) => {
                console.error('unregisterAgent', errorPayload);
                dispatch(handleError(errorPayload, true))
            })
        });
    }
};

export const ACTION_HANDLERS = {
    [SET_MONITORS]: (state, action) => {
        const newState = {...state, monitors: action.data};
        return newState
    },
    [SET_MONITOR_DATA]: (state, action) => {
        const newState = {...state, monitor: action.data};
        return newState
    },
    [ADD_MONITOR]: (state, action) => {
        let monitors = state.monitors;
        monitors.push(action.data);
        const newState = {
            ...state,
            monitors: [...monitors]
        };
        return newState
    },
    [ADD_AGENT_TO_MONITOR]: (state, action) => {
        let agents = state.monitor.agents;
        agents.push(action.agent);

        const newState = {
            ...state,
            monitor: {...state.monitor, agents}
        };
        return newState
    },
    [REMOVE_AGENT_FROM_MONITOR]: (state, action) => {
        const agents = state.monitor.agents.filter(x=>x._id !== action.aid);

        const newState = {
            ...state,
            monitor: {...state.monitor, agents}
        };
        return newState
    },
    [DELETE_MONITOR]: (state, action) => {
        const monitors = state.monitors.filter(m=>m._id !== action.id);

        const newState = {
            ...state,
            monitors: [...monitors]
        };
        return newState
    }
};