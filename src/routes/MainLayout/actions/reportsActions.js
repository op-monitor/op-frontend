import {stateValueExtractor} from '../../../utils'


const SET_REPORTS = 'REPORT.SET_REPORTS';
const SET_REPORT_DATA = 'REPORT.SET_REPORT_DATA';


export const getAllReports = ({mid, aid}) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getReports(mid, aid), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_REPORTS, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getAllReports', errorPayload);
                reject(errorPayload);
            })
        });
    }
};

export const getReport = (id) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getReport(id), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_REPORT_DATA, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getReport', errorPayload);
                reject(errorPayload);
            })
        });
    }
};

export const ACTION_HANDLERS = {
    [SET_REPORTS]: (state, action) => {
        const newState = {...state, reports: action.data};
        return newState
    },
    [SET_REPORT_DATA]: (state, action) => {
        const newState = {...state, report: action.data};
        return newState
    }
};