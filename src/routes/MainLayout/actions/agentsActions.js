import {stateValueExtractor} from '../../../utils'
import {handleError, showInfo} from "../../CoreLayout/actions/CoreActions";
import {closeModal} from "./mainActions";

const SET_AGENTS = 'AGENT.SET_AGENTS';
const SET_AGENT_DATA = 'AGENT.SET_AGENT_DATA';
const ADD_AGENT = 'AGENT.ADD_AGENT';
const DELETE_AGENT = 'AGENT.DELETE_AGENT';


export const getAllAgents = () => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getAgents(), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_AGENTS, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getAllAgents', errorPayload);
                reject(errorPayload);
            })
        });
    }
};

export const getAgent = (id) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();
        return new Promise((resolve, reject) => {
            fetch(urls.getAgent(id), {
                method: 'GET',
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: SET_AGENT_DATA, data: resp.data});
            }).catch((errorPayload) => {
                console.error('getAgent', errorPayload);
                reject(errorPayload);
            })
        });
    }
};

export const addNewAgent = () => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        const modalData = getState().main.modal;
        const packetData = {data: {host: modalData.host, port: Number(modalData.port)}};

        return new Promise((resolve, reject) => {
            fetch(urls.addAgent(), {
                method: 'POST',
                body: JSON.stringify(packetData),
                ...defaultSettings
            }).then((resp) => {
                if (resp.status === 200) {
                    return resp.json()
                }
                else {
                    throw resp
                }
            }).then(resp => {
                dispatch({type: ADD_AGENT, data: resp.data});
                dispatch(showInfo('Added new Agent successfully'));
                dispatch(closeModal());
            }).catch((errorPayload) => {
                console.error('addAgent', errorPayload);
                dispatch(handleError(errorPayload, true))
            })
        });
    }
};

export const deleteAgent = (id) => {
    return (dispatch, getState) => {
        const urls = stateValueExtractor.getUrls(getState());
        const defaultSettings = stateValueExtractor.getDefaultSettings();

        return new Promise((resolve, reject) => {
            fetch(urls.deleteAgent(id),
                {
                    method: 'DELETE',
                    ...defaultSettings
                })
                .then((resp) => {
                    if (resp.status === 200) {
                        return resp.json()
                    }
                    else {
                        throw resp
                    }
                })
                .then(resp => {
                    dispatch({type: DELETE_AGENT, id: id});
                    dispatch(showInfo('Deleted Agent successfully'));
                })
                .catch((errorPayload) => {
                    console.error('deleteAgent', errorPayload);
                    dispatch(handleError(errorPayload, true))
                })
        });
    }
};

export const ACTION_HANDLERS = {
    [SET_AGENTS]: (state, action) => {
        const newState = {...state, agents: action.data};
        return newState
    },
    [SET_AGENT_DATA]: (state, action) => {
        const newState = {...state, agent: action.data};
        return newState
    },
    [ADD_AGENT]: (state, action) => {
        let agents = state.agents;
        agents.push(action.data);
        const newState = {
            ...state,
            agents: [...agents]
        };
        return newState
    },
    [DELETE_AGENT]: (state, action) => {
        const agents = state.agents.filter(m=>m._id !== action.id);

        const newState = {
            ...state,
            agents: [...agents]
        };
        return newState
    }

};