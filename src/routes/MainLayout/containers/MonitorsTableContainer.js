import connect from "react-redux/es/connect/connect";

import {openModal, toggleMenu} from "../actions/mainActions";
import {addNewMonitor, deleteMonitor, getAllMonitors} from "../actions/monitorsActions";

import {MonitorsTable} from "../../../components/Layouts";

const mapStateToProps = state => ({
    monitors: state.main.monitors,
});

const mapDispatchToProps = {
    toggleMenu,
    getAllMonitors,
    addNewMonitor,
    openModal,
    deleteMonitor
};

export default connect(mapStateToProps, mapDispatchToProps)(MonitorsTable);
