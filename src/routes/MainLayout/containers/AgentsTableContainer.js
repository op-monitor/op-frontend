import connect from "react-redux/es/connect/connect";

import {openModal} from "../actions/mainActions";
import {addNewAgent, deleteAgent, getAllAgents} from "../actions/agentsActions";


import {AgentsTable} from "../../../components/Layouts"

const mapStateToProps = state => ({
    agents: state.main.agents
});

const mapDispatchToProps = {
    getAllAgents,
    addNewAgent,
    openModal,
    deleteAgent
};


export default connect(mapStateToProps, mapDispatchToProps)(AgentsTable);
