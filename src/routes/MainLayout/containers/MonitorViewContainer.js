import connect from "react-redux/es/connect/connect";

import {getAllMonitors, getMonitor, registerAgent, unregisterAgent} from "../actions/monitorsActions";
import {openModal} from "../actions/mainActions";
import {getAllAgents} from "../actions/agentsActions";

import {MonitorView} from "../../../components/Layouts"

const mapStateToProps = state => ({
    monitor: state.main.monitor
});

const mapDispatchToProps = {
    getMonitorData: getMonitor,
    openModal,
    registerAgent,
    unregisterAgent,
    getAgents: getAllAgents,
    getMonitors: getAllMonitors
};


export default connect(mapStateToProps, mapDispatchToProps)(MonitorView);
