import connect from "react-redux/es/connect/connect";

import {toggleMenu} from "../actions/mainActions";
import {closeNotification, loadConfig} from "../../CoreLayout/actions/CoreActions";
import {getAllMonitors} from "../actions/monitorsActions";

import App from "../../../components/App"


const mapStateToProps = state => ({
    isMenuOpen: state.main.isMenuOpen,
    urls: state.core.urls,
    isNotificationOpen: state.core.isNotificationOpen,
    notificationMessage: state.core.notificationMessage,
    notificationReason: state.core.notificationReason
});

const mapDispatchToProps = {
    toggleMenu,
    loadConfig,
    getAllMonitors,
    closeNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
