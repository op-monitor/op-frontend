import connect from "react-redux/es/connect/connect";

import {getAgent} from "../actions/agentsActions";
import {getAllReports} from "../actions/reportsActions";

import {AgentView} from "../../../components/Layouts"

const mapStateToProps = state => ({
    agent: state.main.agent,
    reports: state.main.reports,
});

const mapDispatchToProps = {
    getAgentData: getAgent,
    getAllReports: getAllReports
};

export default connect(mapStateToProps, mapDispatchToProps)(AgentView);
