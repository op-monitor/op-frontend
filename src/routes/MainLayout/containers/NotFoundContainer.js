import connect from "react-redux/es/connect/connect";
import NotFound from "../../../components/NotFound";

const mapStateToProps = state => ({});

const mapDispatchToProps = {};


export default connect(mapStateToProps, mapDispatchToProps)(NotFound);
