import connect from "react-redux/es/connect/connect";

import {closeModal, openModal, updateModalInputs} from "../actions/mainActions";

import {PlainModal} from "../../../components/BasicComponents";

const mapStateToProps = (state, ownProps) => ({
    data: state.main.modal,
    isOpen: state.main.isModalOpen,
    title: ownProps.title,
    subtitle: ownProps.subtitle
});

const mapDispatchToProps = {
    updateInputs: updateModalInputs,
    close: closeModal,
    openModal
};


export default connect(mapStateToProps, mapDispatchToProps)(PlainModal);
