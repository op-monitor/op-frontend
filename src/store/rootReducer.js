import {combineReducers} from 'redux';
import {coreReducer, mainReducer} from "../routes/index"


// The keys resemble the actual state[key] values (use later in the containers)
export default combineReducers({
    main: mainReducer,
    core: coreReducer
});