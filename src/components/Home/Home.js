import React from 'react';
import {Link} from "react-router-dom";

import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import CardActions from "@material-ui/core/CardActions/CardActions";
import Button from "@material-ui/core/Button/Button";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import ToolTip from "@material-ui/core/Tooltip"

import gitlabIcon from "../../static/gitlab_icon.png"
import {strings} from "../../utils";

import classes from './Home.module.scss'


class Home extends React.Component {

    monitorCardDesc = (
        <span>An instance that managed several Agents<br/>
        A Monitor is able to send requests to it's registered Agents<br/>
        Each Monitor installed as a `windows-service`
        </span>);

    agentCardDesc = (
        <span>An instance that monitor specific server<br/>
        Agent may be registered to several Monitors<br/>
        Receive commands and send reports to it's registered Monitors<br/>
        Each Agent installed as a `windows-service`
        </span>);

    generateInfoCard = (name, desc, stack = "Python3.6") => {
        return (
            <Card className={classes.infoCard}>
                <CardHeader title={
                    <Typography className={classes.infoCardTitle} variant="h5" component="h2">
                        {strings.capitalize(name)}
                    </Typography>}
                />
                <CardContent>
                    <Typography className={classes.infoCardDesc} color="textSecondary" style={{marginBottom: "12px"}}>
                        {desc}
                    </Typography>
                    <Typography variant="body2" component="p">
                        Stack: {stack}
                    </Typography>
                </CardContent>
                <CardActions className={classes.infoCardActions} style={{alignItems: "space-between"}}>
                    <ToolTip title={"open repo in gitlab"}>
                        <Button size="small">
                            <a href="https://gitlab.com/op-monitor/monitor-service"
                               target='_blank'
                               rel="noopener noreferrer"
                               className={classes.gitlabLink}>
                                <img src={gitlabIcon} className={classes.gitlabIcon} alt=""/>
                                <span>code</span>
                            </a>
                        </Button>
                    </ToolTip>
                    <ToolTip title={`view the active ${name}s`}>
                        <Button size="small">
                            <Link to={`/${name}s`}
                                  style={{textDecoration: 'none', color: "black"}}>Active {name}s</Link>
                        </Button>
                    </ToolTip>
                </CardActions>
            </Card>)
    };

    render() {
        return (
            <div className={classes.wrapper}>
                <div className={classes.headersTitle}>
                    <h1>Welcome to <span>op-monitor</span></h1>
                    <h2>To navigate the site use the following links</h2>
                </div>
                <div className={classes.cardsManuals}>
                    {this.generateInfoCard('monitor', this.monitorCardDesc)}
                    {this.generateInfoCard('agent', this.agentCardDesc)}
                </div>
            </div>
        );
    }
}

export default Home;