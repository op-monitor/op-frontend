import React from 'react'
import connect from "react-redux/es/connect/connect";

import {loadConfig} from "../../routes/CoreLayout/actions/CoreActions";
import {objects} from '../../utils/index'


class Startup extends React.Component {

    componentDidMount() {
        this.props.loadConfig();
    }

    render() {
        return objects.isEmpty(this.props.urls)
            ? (<h1>Loading...</h1>)
            : this.props.children
    }
}

function mapStateToProps(state) {
    return {
        urls: state.core.urls
    };
}

const mapDispatchToProps = {
    loadConfig: loadConfig
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Startup);