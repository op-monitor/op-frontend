import React from 'react'
import PropTypes from 'prop-types';

import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent/SnackbarContent";

export const NotificationIndicator = ({close, isOpen, message, reason}) => {
    const isError = reason === 'error';
    const ERROR_COLOR = '#FF3333';
    const INFO_COLOR = '#8BC34A';
    const textColor = isError ? ERROR_COLOR : INFO_COLOR;
    const styles = {
        color: '#fff',
        fontWeight: '600',
        display: 'flex',
        justifyContent: 'center',
        userSelect: 'none',
        cursor: 'default',
        backgroundColor: textColor
    };

    return (
        <Snackbar
            open={isOpen}
            onClose={close}>
            <SnackbarContent
                style={styles}
                message={message}
            />
        </Snackbar>);
};

NotificationIndicator.propTypes = {
    close: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    reason: PropTypes.string
};

export default NotificationIndicator;
