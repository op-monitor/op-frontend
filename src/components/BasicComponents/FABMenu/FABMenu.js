import React from 'react'
import PropTypes from 'prop-types';

import {
    FloatingMenu,
    MainButton,
    ChildButton,
} from 'react-floating-button-menu';
import CollapseOptionsIcon from '@material-ui/icons/MoreHoriz';
import SpreadOptionsIcon from '@material-ui/icons/MoreVert';
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import AddIcon from '@material-ui/icons/Add';

import classes from "./FABMenu.module.scss"

class FABMenu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isFabOpen: false,
        };
    }

    static generateIcon(type) {
        switch (type) {
            case 'add':
                return <AddIcon className={classes.iconStyle}/>;
            default:
                return <AddIcon className={classes.iconStyle}/>;
        }
    }

    render() {
        const {
            actions
        } = this.props;

        return (
            <FloatingMenu
                slideSpeed={500}
                direction="up"
                spacing={8}
                isOpen={this.state.isFabOpen}
                className={classes.floatingButton}
            >
                <MainButton
                    className={classes.fabStyles}
                    iconResting={<SpreadOptionsIcon className={classes.iconStyle}/>}
                    iconActive={<CollapseOptionsIcon className={classes.iconStyle}/>}
                    onClick={() => this.setState({isFabOpen: !this.state.isFabOpen})}
                    size={56}
                />
                {actions.map(({tooltip, icon, onclick}) =>
                    <ChildButton
                        className={classes.fabStyles}
                        icon={<Tooltip title={tooltip} placement="right"
                                       classes={{tooltip: classes.tooltip}}>
                            {FABMenu.generateIcon(icon)}
                        </Tooltip>}
                        size={40}
                        onClick={onclick}
                    />
                )}
            </FloatingMenu>
        );
    };
}

FABMenu.propTypes = {
    actions: PropTypes.array.isRequired
};

export default FABMenu;
