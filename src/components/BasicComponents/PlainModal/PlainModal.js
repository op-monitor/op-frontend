import React from 'react'
import PropTypes from 'prop-types';

import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import Modal from "@material-ui/core/Modal/Modal";

import classes from "./PlainModal.module.scss"

class PlainModal extends React.Component {

    render() {
        const {
            isOpen,
            close,
            updateInputs,
            title,
            subtitle,
            data: {host, port},
            submitAction
        } = this.props;

        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={isOpen}
                style={{top: '120px'}}
                onClose={close}
            >
                <div className={classes.modalStyle}>
                    <h2 id="simple-modal-title">{title}</h2>
                    <h4 id="simple-modal-title">{subtitle}</h4>
                    {host && <TextField
                        id="host_input"
                        label="Host"
                        value={host}
                        onChange={(event) => updateInputs('host', event.target.value)}
                        margin="normal"
                    />}
                    {port && <TextField
                        id="port_input"
                        label="Port"
                        value={port}
                        onChange={(event) => updateInputs('port', event.target.value)}
                        margin="normal"
                    />}
                    <Button variant="outlined"
                            color="primary"
                            className={classes.modalSubmitButton}
                            onClick={submitAction}
                    >
                        Submit
                    </Button>
                </div>
            </Modal>
        );
    };

}

PlainModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    updateInputs: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    data: PropTypes.object.isRequired,
    submitAction: PropTypes.func.isRequired
};

export default PlainModal;
