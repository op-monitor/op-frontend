import React from 'react'
import PropTypes from 'prop-types';

import {ServersTable} from "../BasicComponents";
import FABMenu from "../../BasicComponents/FABMenu/FABMenu";
import PlainModalContainer from "../../../routes/MainLayout/containers/PlainModalContainer";

import classes from './MonitorsTable.module.scss';

class MonitorsTable extends React.Component {

    componentDidMount() {
        const {getAllMonitors} = this.props;
        getAllMonitors();
    }

    render() {
        const {
            monitors,
            addNewMonitor,
            openModal,
            deleteMonitor
        } = this.props;

        return (
            <div>
                <PlainModalContainer
                    title={'Add new monitor'}
                    subtitle={'Notice! the monitor need to be up and running'}
                    submitAction={addNewMonitor}
                />
                <div className={classes.pageHeader}><h1>Active Monitors</h1></div>
                <ServersTable
                    servers={monitors}
                    serverType="monitor"
                    containedServerType="agent"
                    actions={[{tooltip: 'View Page', icon: 'arrow', onclick: null, navigate: true},
                        {tooltip: 'Delete', icon: 'delete', onclick: deleteMonitor, navigate: false}]}
                />
                <FABMenu actions={[{tooltip: "Add new Monitor", icon: 'add', onclick: openModal}]}/>
            </div>)
    }
}

MonitorsTable.propTypes = {
    getAllMonitors: PropTypes.func.isRequired,
    monitors: PropTypes.array.isRequired,
    addNewAgent: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    deleteMonitor: PropTypes.func.isRequired
};

export default MonitorsTable