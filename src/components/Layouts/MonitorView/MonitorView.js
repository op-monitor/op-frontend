import React from 'react'
import PropTypes from 'prop-types';

import {FABMenu} from "../../BasicComponents";
import {ServerView} from "../BasicComponents";

import PlainModalContainer from "../../../routes/MainLayout/containers/PlainModalContainer";

class MonitorView extends React.Component {

    componentDidMount() {
        const {getMonitorData, getAgents} = this.props;
        const {monitorId} = this.props.match.params;  // Get the params from the dynamic router
        getMonitorData(monitorId);
        getAgents();
    }

    render() {
        const {monitor, registerAgent, openModal, unregisterAgent} = this.props;

        return (
            <div>
                <PlainModalContainer
                    title={'Register agent to Monitor'}
                    subtitle={'Notice! the agent need to be up and running'}
                    submitAction={registerAgent}/>
                <ServerView
                    serverData={monitor}
                    serverType="monitor"
                    containedServerType="agent"
                    tableActions={[{tooltip: 'View Page', icon: 'arrow', onclick: null, navigate: true},
                    {tooltip: 'Unregister', icon: 'unregister', onclick: unregisterAgent, navigate: false}]}
                />
                <FABMenu actions={[{tooltip: "Register existing agent", icon: 'add', onclick: openModal}]}/>
            </div>
        )
    }
}

MonitorView.propTypes = {
    getMonitorData: PropTypes.func.isRequired,
    getAgents: PropTypes.func.isRequired,
    monitor: PropTypes.object.isRequired,
    registerAgent: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    unregisterAgent: PropTypes.func.isRequired
};

export default MonitorView