import AgentsTable from './AgentsTable'
import AgentView from './AgentView'
import MonitorsTable from './MonitorsTable'
import MonitorView from './MonitorView'


export {AgentsTable, AgentView, MonitorsTable, MonitorView}