import React from 'react'
import PropTypes from 'prop-types';

import {ServersTable} from "../BasicComponents";
import FABMenu from "../../BasicComponents/FABMenu/FABMenu";
import PlainModalContainer from "../../../routes/MainLayout/containers/PlainModalContainer";

import classes from './AgentsTable.module.scss';

class AgentsTable extends React.Component {

    componentDidMount() {
        const {getAllAgents} = this.props;
        getAllAgents();
    }

    render() {
        const {
            agents,
            addNewAgent,
            openModal,
            deleteAgent
        } = this.props;

        return (
            <div>
                <PlainModalContainer
                    title={'Add new agent'}
                    subtitle={'Notice! the agent need to be up and running'}
                    submitAction={addNewAgent}/>
                <div className={classes.pageHeader}><h1>Active Agents</h1></div>
                <ServersTable
                    servers={agents}
                    serverType="agent"
                    containedServerType="monitor"
                    actions={[{tooltip: 'View Page', icon: 'arrow', onclick: null, navigate: true},
                        {tooltip: 'Delete', icon: 'delete', onclick: deleteAgent, navigate: false}]}
                />
                <FABMenu actions={[{tooltip: "Add new Agent", icon: 'add', onclick: openModal}]}/>
            </div>)
    }
}

AgentsTable.propTypes = {
    agents: PropTypes.array.isRequired,
    addNewAgent: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    getAllAgents: PropTypes.func.isRequired,
    deleteAgent: PropTypes.func.isRequired
};

export default AgentsTable