import React from 'react'
import PropTypes from 'prop-types';

import Chart from "react-google-charts";
import {ServerView} from "../BasicComponents";
import {strings} from "../../../utils"

import Container from "@material-ui/core/Container/Container";
import Typography from "@material-ui/core/Typography/Typography";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Card from "@material-ui/core/Card/Card";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import Table from "@material-ui/core/Table/Table";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableBody from "@material-ui/core/TableBody/TableBody";

import classes from './AgentView.module.scss';

class AgentView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            timer: null
        }
    }

    componentDidMount() {
        const {getAgentData, getAllReports} = this.props;
        const {agentId} = this.props.match.params;  // Get the params from the dynamic router

        getAgentData(agentId);
        getAllReports({aid: agentId});

        // set timer to retrieve the reports one again after const duration
        let timer = setInterval(()=>getAllReports({aid: agentId}), 2000);
        this.setState({timer});
    }

    componentWillUnmount() {
        clearInterval(this.state.timer);
    }

    static generatePlainCard(title, subtitle, data) {
        return (
            <Card className={classes.plainCard}>
                <CardHeader
                    title={title}
                    subheader={subtitle}
                />
                <CardContent>
                    <Typography variant="h5" component="h2" style={{textAlign: 'center'}}>
                        {data}
                    </Typography>
                </CardContent>
            </Card>);
    }

    static generatePortTableRow(row) {
        return (<TableRow key={row.pid}>
            <TableCell component="th" scope="row">{row.pid}</TableCell>
            <TableCell component="th"
                       scope="row">{row.raddr && row.raddr.ip ? `${row.raddr.ip}:${row.raddr.port}` : '0.0.0.0'}</TableCell>
            <TableCell component="th"
                       scope="row">{row.laddr && row.laddr.ip ? `${row.laddr.ip}:${row.laddr.port}` : '0.0.0.0'}</TableCell>
            <TableCell component="th" scope="row">{row.status}</TableCell>
        </TableRow>)
    }

    static generateProccessTableRow(row) {
        return (<TableRow key={row.pid}>
            <TableCell component="th" scope="row">{row.pid}</TableCell>
            <TableCell component="th" scope="row">{row.name}</TableCell>
            <TableCell component="th" scope="row">{row.username ? row.username : "System"}</TableCell>
        </TableRow>)
    }

    generateTableCard(title, subtitle, columns, rows, generateRow) {
        return (
            <Card className={classes.tableCard}>
                <CardHeader
                    title={title}
                    subheader={subtitle}
                />
                <CardContent>
                    <Table stickyHeader className={classes.table}>
                        <TableHead className={classes.tableHeader}>
                            <TableRow>
                                {columns.map(col=><TableCell align="left">{col}</TableCell>)}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => generateRow(row))}
                        </TableBody>
                    </Table>
                </CardContent>
            </Card>);
    }

    static generateGraphCard(title, subtitle, columns, rows, style = {
        width: '400px',
        height: '400px',
        colors: undefined
    }, customClass = "") {
        return (
            <Card className={classes.graphCard}>
                <CardHeader
                    title={title}
                    subheader={subtitle}
                />
                <CardContent className={`${classes.cardContentRoot} ${customClass}`}>
                    <Chart
                        chartType="AreaChart"
                        width={style.width}
                        height={style.height}
                        legendToggle
                        rows={rows}
                        columns={columns}
                        options={{
                            legend: {position: 'top', maxLines: 3},
                            colors: style.colors ? style.colors : ['rgb(51, 102, 204)']
                        }}
                    />
                </CardContent>
            </Card>);
    }

    static generatePieChartCard(title, subtitle, columns, rows, colors = undefined) {
        return (
            <Card className={`${classes.graphCard} ${classes.pieCard}`}>
                <CardHeader
                    title={title}
                    subheader={subtitle}
                />
                <CardContent className={classes.cardContentRoot}>
                    <Chart
                        width={'350px'}
                        height={'200px'}
                        chartType="PieChart"
                        loader={<div>Loading Chart</div>}
                        data={[
                            columns,
                            ...rows
                        ]}
                        options={{
                            legend: {position: 'top', maxLines: 1},
                            slices: colors
                                ? colors
                                : {
                                    0: {color: 'rgb(220, 57, 18)'},
                                    1: {color: 'rgb(51, 102, 204)'}
                                }
                        }}
                    />
                </CardContent>
            </Card>);
    }

    generateMemoryCharts(memoryReport) {
        const reduceDiskUsage = (usage) => {
            return {used: usage.used, free: usage.free}
        };
        return (memoryReport.data.disks
            .map(disk =>
                AgentView.generatePieChartCard(
                    `Disk ${disk.address}`,
                    `Usage of disk ${disk.address}`,
                    ['Parameter', 'Bytes'],
                    Object.entries(disk.usage ? reduceDiskUsage(disk.usage) : {}))));
    }

    static generateRAMChart(report) {
        const reduceDiskUsage = (usage) => {
            return {used: usage.used, available: usage.available}
        };
        return (AgentView.generatePieChartCard(
            `RAM`,
            `RAM Usage`,
            ['Parameter', 'Bytes'],
            Object.entries(reduceDiskUsage(report.data)),
            {
                0: {color: 'rgb(153, 68, 153)'},
                1: {color: 'rgb(50, 146, 98)'}
            }
        ))
    }

    generateOSCard(title, subtitle, report) {
        return (
            <Card className={classes.osCard}>
                <CardHeader
                    title={title}
                    subheader={subtitle}
                />
                <CardContent style={{display: 'flex', flexDirection: 'column'}}>
                    {Object.entries(report.data).map(([key, value]) =>
                        <Typography variant="subtitle1" component="span"
                                    style={{textAlign: 'left', marginBottom: '5px'}}>
                            <b>{strings.capitalize(key)}</b>: {value}
                        </Typography>)}
                </CardContent>
            </Card>);
    }

    generatePingCards(report) {
        return(
            <div className={classes.pingsSection}>
                {Object.entries(report.data).map(([domain, status]) =>
                    <Card className={status ? classes.pingSuccess : classes.pingFailure}>
                        <CardHeader
                            title={`Ping ${domain}.com`}
                        />
                        <CardContent>
                            <Typography variant="h5" component="h3" style={{textAlign: 'center'}}>
                                {status ? 'success' : 'failure'}
                            </Typography>
                            <Typography variant="subtitle2" component="h3" style={{textAlign: 'center', marginTop: '5px'}}>
                                last updated ${report.creationDate}
                            </Typography>
                        </CardContent>
                    </Card>)}
            </div>
        );
    }

    findLatestReport(reports, type) {
        const filteredReports = reports.filter(x => x.task === type);
        const isReportExists = filteredReports.length > 0;
        const latestReport = isReportExists ? filteredReports.reduce((x, y) => x.creationDate > y.creationDate ? x : y) : {};
        return {exists: isReportExists, report: latestReport, reports: filteredReports};
    }

    render() {
        const {agent, reports} = this.props;

        const isReported = reports.length > 0;

        const {exists: isCPUReport, report: cpuReport, reports: cpuReports} = this.findLatestReport(reports, "CPUMonitor");
        const cpuReportsData = cpuReports.map(x => [new Date(x.creationDate), x.data.cpu]);

        const {exists: isMemoryReport, report: memoryReport} = this.findLatestReport(reports, "MemoryMonitor");
        const {exists: isOSReport, report: osReport} = this.findLatestReport(reports, "OSMonitor");
        const {exists: isRAMReport, report: ramReport, reports: ramReports} = this.findLatestReport(reports, "RAMMonitor");
        const ramReportsData = ramReports.map(x => [new Date(x.creationDate), x.data.used]);

        const {exists: isPortsReport, report: portsReport} = this.findLatestReport(reports, "PortsMonitor");
        const {exists: isProcessesReport, report: processReport} = this.findLatestReport(reports, "ProcessesMonitor");
        const {exists: isPingReport, report: pingReport} = this.findLatestReport(reports, "PingMonitor");

        return (
            <div className={classes.viewWrapper}>
                <ServerView
                    serverData={agent}
                    serverType="agent"
                    containedServerType="monitor"
                    tableActions={[{tooltip: 'View Page', icon: 'arrow', onclick: null, navigate: true}]}
                />
                <div className={classes.tasksWrapper}>
                    <Typography variant="overline" display="block" gutterBottom>Tasks</Typography>
                    {isReported
                        ? <Container className={classes.container}>
                            {isOSReport ? this.generateOSCard("OS", "OS parameters", osReport) : ''}
                            <div className={classes.coresCard}>
                                {isCPUReport ? AgentView.generatePlainCard("Cores", "Number of virtual cores", cpuReport.data.cores) : ''}
                                {isCPUReport ? AgentView.generatePlainCard("Physical Cores", "Number of physical cores", cpuReport.data.physicalCores) : ''}
                            </div>
                            {isCPUReport ? AgentView.generateGraphCard("CPU Monitor", "latest CPU values", [
                                    {
                                        label: "Time",
                                        type: "date"
                                    },
                                    {
                                        label: "CPU",
                                        type: "number"
                                    }
                                ], cpuReportsData,
                                {width: '500px', height: '300px'},
                                classes.cpuCard) : ''}
                            {isRAMReport ? AgentView.generateRAMChart(ramReport) : ''}
                            {isRAMReport ? AgentView.generateGraphCard("RAM Monitor", "latest RAM values", [
                                    {
                                        label: "Time",
                                        type: "date"
                                    },
                                    {
                                        label: "RAM",
                                        type: "number"
                                    }
                                ], ramReportsData,
                                {width: '720px', height: '200px', colors: ['rgb(153, 68, 153)']},
                                classes.ramCard) : ''}
                            {isMemoryReport ? this.generateMemoryCharts(memoryReport) : ''}
                            {isPortsReport ? this.generateTableCard(
                                'Ports Table',
                                'The active ports',
                                ["Process ID", "Remote Address", "Local Address", "Status"],
                                portsReport.data.connections,
                                AgentView.generatePortTableRow) : ''}
                            {isProcessesReport ? this.generateTableCard(
                            'Processes Table',
                            'The active processes',
                            ["Process ID", "Process Name", "Registered user"],
                            processReport.data.processes,
                            AgentView.generateProccessTableRow) : ''}
                            {isPingReport ? this.generatePingCards(pingReport) : ""}
                        </Container>
                        : <h3>No data</h3>}
                </div>
            </div>
        )
    }
}

AgentView.propTypes = {
    getAgentData: PropTypes.func.isRequired,
    getAllReports: PropTypes.func.isRequired,
    agent: PropTypes.object.isRequired,
    reports: PropTypes.array.isRequired
};

export default AgentView