import React from 'react'
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import DeleteIcon from "@material-ui/icons/Delete";

import {strings} from "../../../../utils"

import classes from './ServersTable.module.scss';

class ServersTable extends React.Component {

    static chooseIcon(icon){
        switch (icon) {
            case "arrow":
                return <PlayArrowIcon/>;
            case "delete":
                return <DeleteIcon/>;
            default:
                return <PlayArrowIcon/>;
        }
    }

    generateActionButton(row) {
        const {serverType, actions} = this.props;
        return (actions.map(({tooltip, icon, onclick, navigate}) => (
            <Tooltip title={tooltip} placement="bottom">
                <IconButton color="primary"
                            className={classes.button}
                            aria-label="add an alarm"
                            onClick={onclick ? () => onclick(row._id) : null}
                >
                    {!navigate ? ServersTable.chooseIcon(icon)
                        :
                        <Link to={`/${serverType}/${row._id}`}>
                            {ServersTable.chooseIcon(icon)}
                        </Link>}
                </IconButton>
            </Tooltip>
        )))
    }

    render() {
        const {servers, containedServerType} = this.props;
        return (
            <Paper className={classes.paperRoot}>
                <Table stickyHeader className={classes.table}>
                    <TableHead className={classes.tableHeader}>
                        <TableRow>
                            <TableCell>Address</TableCell>
                            <TableCell align="left">ID</TableCell>
                            <TableCell align="left">Created Time</TableCell>
                            <TableCell align="left">Registered {strings.capitalize(containedServerType)}s</TableCell>
                            <TableCell align="left">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {servers.map(row => (
                            <TableRow key={row._id}>
                                <TableCell component="th" scope="row">{`${row.host}:${row.port}`}</TableCell>
                                <TableCell align="left">{row._id}</TableCell>
                                <TableCell align="left">{row.creationDate}</TableCell>
                                <TableCell align="left">{row[`${containedServerType}s`].length}</TableCell>
                                <TableCell align="left">
                                    {this.generateActionButton(row)}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

ServersTable.propTypes = {
    servers: PropTypes.array.isRequired,
    serverType: PropTypes.string.isRequired,
    containedServerType: PropTypes.string.isRequired,
    actions: PropTypes.array.isRequired
};

export default ServersTable