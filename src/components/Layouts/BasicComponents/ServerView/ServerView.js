import React from 'react'
import PropTypes from 'prop-types';

import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import IconButton from "@material-ui/core/IconButton/IconButton";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import Avatar from "@material-ui/core/Avatar/Avatar";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import Divider from "@material-ui/core/Divider/Divider";
import ListItemAvatar from "@material-ui/core/ListItemAvatar/ListItemAvatar";
import Typography from "@material-ui/core/Typography/Typography";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

import AddressIcon from "@material-ui/icons/EventNote"
import CreatedDateIcon from "@material-ui/icons/AccessTime"
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import IDIcon from "@material-ui/icons/AccountCircle";
import UnregisterIcon from "@material-ui/icons/RemoveCircle";

import {strings} from "../../../../utils/";
import {Link} from "react-router-dom";

import classes from './ServerView.module.scss';

class ServerView extends React.Component {

    static chooseIcon(icon){
        switch (icon) {
            case "arrow":
                return <PlayArrowIcon/>;
            case "unregister":
                return <UnregisterIcon/>;
            default:
                return <PlayArrowIcon/>;
        }
    }

    generateActionButton(row) {
        const {containedServerType, tableActions} = this.props;
        return (tableActions.map(({tooltip, icon, onclick, navigate}) => (
            <Tooltip title={tooltip} placement="bottom">
                <IconButton color="primary"
                            className={classes.button}
                            aria-label="add an alarm"
                            onClick={onclick ? () => onclick(row._id) : null}
                >
                    {!navigate ? ServerView.chooseIcon(icon)
                        :
                        <Link to={`/${containedServerType}/${row._id}`}>
                            {ServerView.chooseIcon(icon)}
                        </Link>}
                </IconButton>
            </Tooltip>
        )));
    }

    renderRegisteredTable(rows) {
        return (<Paper>
            <Table stickyHeader className={classes.table}>
                <TableHead className={classes.tableHeader}>
                    <TableRow>
                        <TableCell>Address</TableCell>
                        <TableCell align="left">ID</TableCell>
                        <TableCell align="left">Actions</TableCell>
                        {/* TODO: add number of reports*/}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row._id}>
                            <TableCell component="th" scope="row">{`${row.host}:${row.port}`}</TableCell>
                            <TableCell align="left">{row._id}</TableCell>
                            <TableCell align="left" className={classes.tableActions}>
                                {this.generateActionButton(row)}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>)
    };

    render() {
        // TODO: change the variables name , without the `server` prefix
        const {serverType, serverData, containedServerType} = this.props;
        const containedTypeKey = `${containedServerType}s`;
        const address = `${serverData.host}:${serverData.port}`;

        return (
            <div>
                <div className={classes.pageHeader}><h1>{strings.capitalize(serverType)} View</h1></div>

                <div className={classes.serverData}>
                    <div className={classes.serverMetadata}>
                        <Typography variant="overline" display="block" gutterBottom>Metadata</Typography>
                        <List>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <IDIcon/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary='ID' secondary={serverData._id}/>
                            </ListItem>
                            <Divider variant="inset" component="li"/>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <AddressIcon/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary='Address' secondary={address}/>
                            </ListItem>
                            <Divider variant="inset" component="li"/>
                            <ListItem>
                                <ListItemAvatar>
                                    <Avatar>
                                        <CreatedDateIcon/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary="Created Date" secondary={serverData.creationDate}/>
                            </ListItem>
                        </List>
                    </div>
                    <div className={classes.registeredServers}>
                        <Typography variant="overline" display="block" gutterBottom>{`Registered ${containedTypeKey}`}</Typography>
                        {this.renderRegisteredTable(serverData[containedTypeKey])}
                    </div>
                </div>
            </div>
        )
    }
}

ServerView.propTypes = {
    containedServerType: PropTypes.string.isRequired,
    serverType: PropTypes.string.isRequired,
    serverData: PropTypes.object.isRequired,
    tableActions: PropTypes.array.isRequired
};

export default ServerView