import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Drawer from "@material-ui/core/Drawer/Drawer";
import Typography from "@material-ui/core/Typography/Typography";
import MenuIcon from '@material-ui/icons/Menu';

import { NotificationIndicator } from '../BasicComponents'

import classes from './App.module.scss';


class App extends React.Component {

    render() {
        const {
            toggleMenu,
            isMenuOpen,
            isNotificationOpen,
            notificationMessage,
            notificationReason,
            closeNotification
        } = this.props;
        return (
            <div className={classes.App}>
                <AppBar position="static" classes={{root: classes.AppBarRoot}}>
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={toggleMenu}>
                            <MenuIcon />
                        </IconButton>
                        <Link to={`/home/`} style={{textDecoration: 'none', color: "white"}}>
                            <Typography variant="overline" display="block">op-monitor</Typography>
                        </Link>
                    </Toolbar>
                </AppBar>
                <Drawer open={isMenuOpen} onClose={()=>toggleMenu(false)}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={()=>toggleMenu(false)}
                        onKeyDown={()=>toggleMenu(false)}
                    >
                    </div>
                </Drawer>
                <NotificationIndicator
                    isOpen={isNotificationOpen}
                    message={!notificationMessage ? '' : notificationMessage}
                    reason={notificationReason}
                    close={closeNotification}
                />
            </div>
        );
    }
}

App.propTypes = {
    toggleMenu: PropTypes.func.isRequired,
    isMenuOpen: PropTypes.bool.isRequired,
    isNotificationOpen: PropTypes.bool.isRequired,
    notificationMessage: PropTypes.string,
    notificationReason: PropTypes.string,
    closeNotification: PropTypes.func.isRequired
};

export default App;