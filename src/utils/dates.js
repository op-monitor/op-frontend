import * as moment from 'moment';

export const formatDate = (str) => moment(str, "YYYY-DD-MMTHH:mm:ss.SSSZ");

export const formatToDate = (str) => formatDate(str).toDate();