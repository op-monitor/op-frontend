const init = (apiHost) => {
    return {
        apiHost: apiHost,
        getMonitors: () => `${apiHost}/api/monitor/`,
        getMonitor: (id) => `${apiHost}/api/monitor/${id}?populate`,
        addMonitor: () => `${apiHost}/api/monitor/`,
        deleteMonitor: (id) => `${apiHost}/api/monitor/${id}`,

        registerAgentToMonitor: (mid, aid) => `${apiHost}/api/monitor/${mid}/agent/${aid}?action=register`,
        unregisterAgentToMonitor: (mid, aid) => `${apiHost}/api/monitor/${mid}/agent/${aid}?action=unregister`,

        getAgents: () => `${apiHost}/api/agent/`,
        getAgent: (id) => `${apiHost}/api/agent/${id}?populate`,
        addAgent: () => `${apiHost}/api/agent/`,
        deleteAgent: (id) => `${apiHost}/api/agent/${id}`,

        getReports: (mid=null, aid=null) => `${apiHost}/api/report/?populate${mid ? `&monitor=${mid}` : ''}${aid ? `&agent=${aid}` : ''}`,
        getReport: (id) => `${apiHost}/api/agent/${id}?populate`,

        updateMonitor: (id) => `${apiHost}/api/monitor/${id}`
    }
};

export default (apiHost) => {
    if (!apiHost) {
        throw new Error('Can not initialize config. ApiHost is undefined')
    }

    return init(apiHost)
}