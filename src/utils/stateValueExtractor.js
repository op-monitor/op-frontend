export const getUrls = (state) => state.core.urls;

export const getDefaultSettings = () => {
    return {
        mode: 'cors',
        cache: 'no-cache',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }
};